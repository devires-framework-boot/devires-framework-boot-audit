package br.com.devires.framework.boot.audit.entity;

import br.com.devires.framework.boot.audit.interceptor.EntityChangeType;
import br.com.devires.framework.boot.audit.model.ChangeDetails;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import java.util.Date;

@Data
@EqualsAndHashCode(of = {"auditLogId"})
@Entity
@Table(name = "audit_log")
public class AuditLogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long auditLogId;

    private String entityName;

    private Long entityId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date changedAt;

    @Enumerated(EnumType.STRING)
    private EntityChangeType changeType;

    private String userId;

    private String ipAddress;

    @Type(JsonStringType.class)
    @Column(columnDefinition = "json")
    private ChangeDetails changeDetails;

}
