package br.com.devires.framework.boot.audit.service;

import br.com.devires.framework.boot.audit.entity.AuditLogEntity;
import br.com.devires.framework.boot.audit.interceptor.EntityChange;
import br.com.devires.framework.boot.audit.mapper.AuditLogMapper;
import br.com.devires.framework.boot.audit.model.AuditLog;
import br.com.devires.framework.boot.audit.repository.AuditLogRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class AuditLogService {

    @Autowired
    private AuditLogRepository auditLogRepository;

    @Autowired
    private AuditLogMapper auditLogMapper;

    public List<AuditLog> createAuditLog(List<EntityChange> entityChanges) {
        List<AuditLog> auditLogs = new ArrayList<>();
        if (entityChanges != null && !entityChanges.isEmpty()) {
            for (EntityChange entityChange : entityChanges) {
                AuditLog auditLog = createAuditLog(entityChange);
                if (auditLog != null) {
                    auditLogs.add(auditLog);
                }
            }
        }
        return auditLogs;
    }

    public AuditLog createAuditLog(EntityChange entityChange) {
        Long entityId = entityChange.getEntityId();
        if (entityId != null) {
            AuditLogEntity auditLogEntity = new AuditLogEntity();
            auditLogEntity.setUserId(getLoggerUserPrincipalName());
            auditLogEntity.setIpAddress(getRemoteIpAddress());
            auditLogEntity.setChangedAt(new Date());
            auditLogEntity.setEntityName(entityChange.getEntityName());
            auditLogEntity.setEntityId(entityId);
            auditLogEntity.setChangeType(entityChange.getChangeType());
            auditLogEntity.setChangeDetails(entityChange.getChangeDetails());
            auditLogEntity = auditLogRepository.save(auditLogEntity);
            return auditLogMapper.toModel(auditLogEntity);
        }
        return null;
    }

    private String getLoggerUserPrincipalName() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        Principal principal = request.getUserPrincipal();
        if (principal != null) {
            return principal.getName();
        }
        return null;
    }

    private String getRemoteIpAddress() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String ipAddress = request.getHeader("X-Forwarded-For");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        return ipAddress;
    }

}
