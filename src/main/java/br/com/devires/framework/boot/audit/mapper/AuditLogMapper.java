package br.com.devires.framework.boot.audit.mapper;

import br.com.devires.framework.boot.audit.entity.AuditLogEntity;
import br.com.devires.framework.boot.audit.model.AuditLog;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AuditLogMapper {

    AuditLog toModel(AuditLogEntity entity);

}
