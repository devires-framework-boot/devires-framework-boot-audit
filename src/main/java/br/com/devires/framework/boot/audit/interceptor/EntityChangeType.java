package br.com.devires.framework.boot.audit.interceptor;

public enum EntityChangeType {

    CREATE, UPDATE, DELETE;

}

