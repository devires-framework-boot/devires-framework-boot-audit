package br.com.devires.framework.boot.audit.interceptor;

import br.com.devires.framework.boot.audit.service.AuditLogService;
import br.com.devires.framework.boot.commons.helper.AutowireHelper;
import br.com.devires.framework.boot.commons.utils.AnnotationUtils;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Interceptor;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
public class AuditLogInterceptor implements Interceptor {

    private List<EntityChange> entityChanges = new ArrayList<>();

    @Autowired
    private AuditLogService auditLogService;

    @Override
    public boolean onSave(Object entity, Object id, Object[] state, String[] propertyNames, Type[] types) {
        if (isAuditable(entity)) {
            entityChanges.add(new EntityChange(entity, state, null, propertyNames, types, EntityChangeType.CREATE));
        }
        return true;
    }

    @Override
    public boolean onFlushDirty(Object entity, Object id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
        if (isAuditable(entity)) {
            entityChanges.add(new EntityChange(entity, currentState, previousState, propertyNames, types, EntityChangeType.UPDATE));
        }
        return true;
    }

    @Override
    public void onDelete(Object entity, Object id, Object[] state, String[] propertyNames, Type[] types) {
        if (isAuditable(entity)) {
            entityChanges.add(new EntityChange(entity, state, null, propertyNames, types, EntityChangeType.DELETE));
        }
    }

	@Override
    public void postFlush(Iterator entities) {
        try {
            AutowireHelper.autowire(this, this.auditLogService);
            auditLogService.createAuditLog(entityChanges);
        } catch (Exception e) {
            log.error("Error creating Audit Log", e);
        } finally {
            entityChanges.clear();
        }
    }

    private boolean isAuditable(Object entity) {
        return AnnotationUtils.containsAnnotation(entity, Auditable.class);
    }

}
