package br.com.devires.framework.boot.audit.interceptor;

import br.com.devires.framework.boot.audit.model.ChangeDetails;
import br.com.devires.framework.boot.audit.model.FieldChange;
import br.com.devires.framework.boot.commons.utils.AnnotationUtils;
import br.com.devires.framework.boot.commons.utils.HibernateUtils;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.type.Type;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
public class EntityChange {

    private static final String MASK_FIELD_VALUE = "***";

    private Object entity;

    private Object[] currentState;

    private Object[] previousState;

    private String[] propertyNames;

    private Type[] types;

    private EntityChangeType changeType;

    public Long getEntityId() {
        Object detachedEntity = HibernateUtils.initializeAndUnproxy(entity);
        Field auditKeyField = getAuditKeyField(detachedEntity.getClass());
        if (auditKeyField != null) {
            Long entityId = null;
            try {
                entityId = (Long) auditKeyField.get(detachedEntity);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            return entityId;
        }
        return null;
    }

    public String getEntityName() {
        Object detachedEntity = HibernateUtils.initializeAndUnproxy(entity);
        return detachedEntity.getClass().getSimpleName();
    }

    public ChangeDetails getChangeDetails() {
        Set<FieldChange> fieldChanges = null;
        if (changeType == EntityChangeType.CREATE) {
            fieldChanges = getFieldChangesForCreate();
        } else if (changeType == EntityChangeType.UPDATE) {
            fieldChanges =  getFieldChangesForUpdate();
        }
        if (fieldChanges != null && !fieldChanges.isEmpty()) {
            return new ChangeDetails(fieldChanges);
        }
        return null;
    }

    private Set<FieldChange> getFieldChangesForCreate() {
        Auditable auditableAnnotation = AnnotationUtils.getAnnotation(entity, Auditable.class);
        Set<FieldChange> fieldChanges = new HashSet<>();
        for (int i = 0; i < propertyNames.length; i++) {
            if (isSimpleType(types[i])) {
                FieldChange fieldChange = new FieldChange();
                fieldChange.setField(propertyNames[i]);
                fieldChange.setFrom(getNullSafeString(null));
                if (ArrayUtils.contains(auditableAnnotation.fieldsToMask(), propertyNames[i])) {
                    fieldChange.setTo(MASK_FIELD_VALUE);
                } else {
                    fieldChange.setTo(getNullSafeString(currentState[i]));
                }
                fieldChanges.add(fieldChange);
            }
        }
        return fieldChanges;
    }

    private Set<FieldChange> getFieldChangesForUpdate() {
        Auditable auditableAnnotation = AnnotationUtils.getAnnotation(entity, Auditable.class);
        Set<FieldChange> fieldChanges = new HashSet<>();
        for (int i = 0; i < propertyNames.length; i++) {
            if (isChanged(currentState[i], previousState[i]) && isSimpleType(types[i])) {
                FieldChange fieldChange = new FieldChange();
                fieldChange.setField(propertyNames[i]);
                if (ArrayUtils.contains(auditableAnnotation.fieldsToMask(), propertyNames[i])) {
                    fieldChange.setFrom(MASK_FIELD_VALUE);
                    fieldChange.setTo(MASK_FIELD_VALUE);
                } else {
                    fieldChange.setFrom(getNullSafeString(previousState[i]));
                    fieldChange.setTo(getNullSafeString(currentState[i]));
                }
                fieldChanges.add(fieldChange);
            }
        }
        return fieldChanges;
    }

    public <T> Field getAuditKeyField(Class<T> cls) {
        Class<?> clazz = cls;
        do {
            for (Field field : clazz.getDeclaredFields()) {
                if (AnnotationUtils.containsAnnotation(field, Id.class)) {
                    field.setAccessible(true);
                    return field;
                }
            }
        } while ((clazz = clazz.getSuperclass()) != null);
        return null;
    }

    private boolean isChanged(Object currentState, Object previousState) {
        return (previousState == null && currentState != null)                      // nothing to something
                || (previousState != null && currentState == null)                  // something to nothing
                || (previousState != null && !previousState.equals(currentState));  // something to something else
    }

    private boolean isSimpleType(Type type) {
        return !type.isAssociationType() && !type.isCollectionType() && !type.isComponentType();
    }

    private String getNullSafeString(Object obj) {
        return obj != null ? obj.toString() : null;
    }

}
