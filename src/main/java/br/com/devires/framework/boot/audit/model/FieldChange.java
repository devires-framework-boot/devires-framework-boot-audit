package br.com.devires.framework.boot.audit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FieldChange {

    private String field;

    private String from;

    private String to;

}
