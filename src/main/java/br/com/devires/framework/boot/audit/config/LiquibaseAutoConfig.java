package br.com.devires.framework.boot.audit.config;

import liquibase.change.DatabaseChange;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.sql.DataSource;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass({SpringLiquibase.class, DatabaseChange.class})
@ConditionalOnProperty(prefix = "spring.liquibase", name = "enabled", matchIfMissing = true)
@EnableConfigurationProperties(LiquibaseProperties.class)
public class LiquibaseAutoConfig {

    private static final String DB_CHANGELOG = "classpath:/audit/liquibase/changelog-master.xml";
    private static final String DB_CHANGELOG_TABLE = "DBCHGLOG_AUDIT";
    private static final String DB_CHANGELOG_LOCK_TABLE = "DBCHGLCK_AUDIT";

    private final LiquibaseProperties properties;

    public LiquibaseAutoConfig(LiquibaseProperties properties) {
        this.properties = properties;
    }

    @Bean
    public SpringLiquibase liquibase(ObjectProvider<DataSource> dataSource, @LiquibaseDataSource ObjectProvider<DataSource> liquibaseDataSource) {
        return new LiquibaseAutoConfiguration.LiquibaseConfiguration(properties).liquibase(dataSource, liquibaseDataSource);
    }

    @Bean("liquibaseAudit")
    @DependsOn("liquibase")
    public SpringLiquibase liquibaseAudit(DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setDatabaseChangeLogTable(DB_CHANGELOG_TABLE);
        liquibase.setDatabaseChangeLogLockTable(DB_CHANGELOG_LOCK_TABLE);
        liquibase.setChangeLog(DB_CHANGELOG);
        return liquibase;
    }

}
