package br.com.devires.framework.boot.audit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChangeDetails {

    private Set<FieldChange> fieldChanges;

}
