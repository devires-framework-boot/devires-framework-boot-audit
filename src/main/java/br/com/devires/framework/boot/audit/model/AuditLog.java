package br.com.devires.framework.boot.audit.model;

import br.com.devires.framework.boot.audit.interceptor.EntityChangeType;
import lombok.Data;

import java.util.Date;

@Data
public class AuditLog {

    private Long auditLogId;

    private String entityName;

    private Long entityId;

    private Date changedAt;

    private EntityChangeType changeType;

    private String userId;

    private String ipAddress;

    private ChangeDetails changeDetails;

}
